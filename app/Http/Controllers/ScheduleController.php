<?php

namespace App\Http\Controllers;
use App\Schedule;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    protected $table = 'unime';
    public function index()
    {
        $id= auth()->user()->id;
        $data = Schedule::
        //
        where('user_id', $id )->get();;
        //$data= Schedule::latest()->paginate(5);
        return view('index_schedule', compact('data'));
       // -> with ('i', (request()->input('page',1)-1) * 5);
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create_schedule');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
        $request->validate ( [
            'course_id' => 'required'
        ]);
      
        $form_data = array(
            'course_id'   => $request->course_id,
            'user_id'  =>  $request->user_id ,
            
        );

        $rec = Schedule::where('course_id', $request->course_id)->where( 'user_id', $request->user_id)->first();
            if ($rec === null) {
                Schedule::create($form_data);
                return redirect('course')->with('success', 'Registered successfully.');
            }
        //Schedule::create($form_data);
        return redirect('course')->with('success', 'Already Registered !');


    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
