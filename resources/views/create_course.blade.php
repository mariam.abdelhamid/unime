@extends('parent')

@section('main')
@if($errors->any())

<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li> {{ $error }} </li>
        @endforeach

    </ul>
</div>
@endif

<div align="right">
    <a href="{{ route('course.index')}}"> Back </a>
</div>


<form method="post" action="{{ route('course.store')}}" enctype="multipart/form-data">
    @csrf
    <div>
    <label> Enter Course Name </label>
        <div>
            <input type="text" name="name" />
        </div>
    </div>

    <br/>
    <br/>

    <div>
    <label> Enter Credit Hours </label>
        <div>
            <input type="text" name="credit" />
        </div>
    </div>

    <br/>
    <br/>

    <div>
        <input type="submit" name="add" />  
    </div>

</form>
    
@endsection