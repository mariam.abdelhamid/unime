@extends('parent')

@section('main')
@if($errors->any())

<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li> {{ $error }} </li>
        @endforeach

    </ul>
</div>
@endif

<div align="right">
    <a href="{{ route('course.index')}}"> Back </a>
</div>


<form method="post" action="{{ route('schedule.store')}}" enctype="multipart/form-data">
    @csrf
    <div>
    <label> Enter User ID </label>
        <div>
            <input type="text" name="user_id" />
        </div>
    </div>

    <br/>
    <br/>

    <div>
    <label> Enter Course ID </label>
        <div>
            <input type="text" name="course_id" />
        </div>
    </div>

    <br/>
    <br/>

    <div>
        <input type="submit" name="add" />  
    </div>

</form>
    
@endsection