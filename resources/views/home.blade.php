@extends('layouts.app')

@section('content')
<div class="container">
<div >
<a href="{{ url('/welcome') }}" style="margin-left:10%;">back</a>
</div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" style="background-color:pink;">
                <div class="card-header" style="background-color:grey;">Dashboard</div>

                <div class="card-body" style="margin-left:45%">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Welcome !
                </div>

                <div class="panel-body" style="margin-left: 25%;  width: 70%;">
            
                    <tr>
                        <td> <p> Role: <strong>{{ Auth::user()->role }}</strong> </p></td>
                        <td><p> Name: <strong>{{ Auth::user()->name }}</strong></p> </td>
                        <td><p> Email: <strong>{{ Auth::user()->email }}</strong></p> </td>
                    </tr>
                
                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
