@extends('parent')

@section('main')


<div align="left">
<a href="{{ url('/welcome') }}">Back</a>
</div>


@if($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif





<table class="table table-bordered table-striped">
    <tr>
      
        <th width="40%"> Course Name </th>
        <th width="40%"> Credit Hours </th>
        <th width="20%"> Actions </th>
        
    </tr>
    @foreach($data as $row)
    
    <tr>
        <td>{{ $row->name }}</td>
        <td>{{ $row->credit }}</td>
        <td>
            <div>
           
                <a href="{{route('course.show', $row->id )}}">Show</a>
                <form action= "{{ route('schedule.store')}}" method="post">
                @csrf
                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}"> 
                    <input type="hidden" name="course_id" value="{{ $row->id }}"> 
                    <input type="submit" name="register" value="register">
                </form>
                
            </div>
        </td>

    </tr>
  

    @endforeach
</table>
{!! $data->links()!!}
@endsection