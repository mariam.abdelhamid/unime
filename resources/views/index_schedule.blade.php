@extends('parent')

@section('main')


<div align="left">
<a href="{{ url('/welcome') }}">Back</a>
</div>


@if($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif





<table class="table table-bordered table-striped">
    <tr>
        <th width="8%"> User Id </th>
        <th width="32%"> User Name </th>
        <th width="8%"> Course Id </th>
        <th width="32%"> Course Name </th>
        <th width="20%"> Course Credit Hours </th>
        
        
        
    </tr>
    @foreach($data as $row)
    <tr>
        <td>{{ $row->user_id }}</td>
        <td> {{auth()->user()->name}} </td>
        <td>{{ $row->course_id }}</td>
        <td> {{ App\Course::where('id', $row->course_id )->first()->name}}</td>
        <td>{{App\Course::where('id', $row->course_id )->first()->credit}} </td>
        

    </tr>

    @endforeach
</table>

@endsection