<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
//Route::post('/schedule', 'ScheduleController@store')->name('schedule.store');

Route::resource('course','CourseController');
Route::resource('schedule','ScheduleController')->middleware('isStudent');
Route::view('/course/create', 'create_course')->middleware('isAdmin');
//Route::view('/course', 'index_course');

//->middleware('auth');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/welcome','HomeController@welcome')->name('welcome');
//Route::get('/course/create','HomeController@create_course')->name('create');


